package pack;

public class Pelicula extends Filmografia{
    
    public Pelicula(String titulo, String genero, int año){
        super(titulo, genero, año);
    }
    @Override
    public String toString(){
        return "Titulo: " + titulo + " /// Genero: " + genero + " /// Año: " + año;
    }
}