package pack;

public class Serie extends Filmografia{
    
    int temporada;

    public Serie(String titulo, String genero, int año, int temporada){
        super(titulo, genero, año);
        this.temporada = temporada;
    }
    public int getTemporada(){
        return temporada;
    }
    @Override
    public String toString(){
        return "Serie: " + titulo + " /// " + genero + " /// Año: " + año + " /// " + temporada + " temporadas";
    }
}        
