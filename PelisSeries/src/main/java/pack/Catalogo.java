package pack;

import java.util.ArrayList;

public class Catalogo{
    
    ArrayList<Filmografia>  listado = new ArrayList();

    public static void main(String[] args){
        
        Catalogo catalogo = new Catalogo();
        
        catalogo.cargarCatalogo(new Pelicula("Top Secret!","Comedia",1984));
        catalogo.cargarCatalogo(new Pelicula("The Truman Show","Drama",1998));
        catalogo.cargarCatalogo(new Pelicula("Wind River","Drama",2017));
        catalogo.cargarCatalogo(new Pelicula("Avangers: Infinity War","Acción",2018));
        
        catalogo.cargarCatalogo(new Serie("Friends","Sitcom",1994,10));
        catalogo.cargarCatalogo(new Serie("Breaking Bad","Drama",2008,7));
        catalogo.cargarCatalogo(new Serie("Scrubs","Comedia",2001,6));
        
        catalogo.mostrarCatalogo();
        
    }
    
    private void cargarCatalogo(Filmografia arg){
            listado.add(arg);
    
    }
    
    private void mostrarCatalogo(){

        listado.forEach((x) -> {
            System.out.println(x.toString());
        });
    }
}